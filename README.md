# Docker Compose restart test

When using *deploy.restart_policy.max_attempts* it gets ignored by Docker Compose (see warning in following output).

*Check branch restart-without-deploy to see results for restart without deploy prefix*

~~~docker
 $ docker rm docker-compose-restart_app_1 && docker rmi docker-compose-restart-test && docker-compose build --build-arg no-cache=true && docker-compose up
docker-compose-restart_app_1
Untagged: docker-compose-restart-test:latest
Deleted: sha256:32cecfe9439bbf2a86ead42135fb439837fa43f86027b6cc707918e71ffa37f4
Deleted: sha256:3592dda09c67444b56402bbe57f20553b866300acfa941c0c20732f2094344db
Deleted: sha256:8048ee5945acccffbf2c399b7d979bf48e76695dc541b6d53ae261d5b3a3d7c9
WARNING: Some services (app) use the 'deploy' key, which will be ignored. Compose does not support 'deploy' configuration - use `docker stack deploy` to deploy to a swarm.
Building app
Step 1/4 : FROM ruby:2.7.1-slim
 ---> 94c23f56e182
Step 2/4 : WORKDIR /usr/src/app
 ---> Using cache
 ---> 17231e766c70
Step 3/4 : COPY app.rb .
 ---> 316647c6b53f
Step 4/4 : CMD ruby app.rb
 ---> Running in f718e3e54d2d
Removing intermediate container f718e3e54d2d
 ---> 0cfd38e2f39d

[Warning] One or more build-args [no-cache] were not consumed
Successfully built 0cfd38e2f39d
Successfully tagged docker-compose-restart-test:latest
WARNING: Some services (app) use the 'deploy' key, which will be ignored. Compose does not support 'deploy' configuration - use `docker stack deploy` to deploy to a swarm.
Creating docker-compose-restart_app_1 ... done
Attaching to docker-compose-restart_app_1
app_1  |   1. Starting app...
docker-compose-restart_app_1 exited with code 1
~~~

Logs after 1 minute
~~~docker
 $ docker logs docker-compose-restart_app_1
  1. Starting app...
~~~
