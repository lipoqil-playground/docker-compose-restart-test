FROM ruby:2.7.1-slim

WORKDIR /usr/src/app

COPY app.rb .

CMD ruby app.rb
