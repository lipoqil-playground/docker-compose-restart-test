state = File.open('state', File::RDWR | File::CREAT)
attempt_number = state.read.to_i + 1

puts "#{attempt_number.to_s.rjust(3, ' ')}. Starting app..."
sleep 3

File.write 'state', attempt_number
exit 1
